using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TheShop.Domains;
using TheShop.Services;

namespace TheShopTests
{
    [TestClass]
    public class TheShopTests
    {
        private const string _findArticleCouldNotFind = "Could not find article";
        private const string _sellArticleArgumentNull = "Value cannot be null. (Parameter 'article')";
        private const string _sellArticleCouldNotSave = "Could not save article";

        [TestMethod]
        public void TestFindArticle()
        {
            var expectedArticle = new Article
            {
                Id = 1,
                Name = "Article from supplier1",
                Price = 458
            };

            var shopService = new ShopService();
            var actualArticle = shopService.FindArticle(1, 500);

            Assert.AreEqual(expectedArticle.Id, actualArticle.Id);
            Assert.AreEqual(expectedArticle.Name, actualArticle.Name);
            Assert.AreEqual(expectedArticle.Price, actualArticle.Price);
        }

        [TestMethod]
        public void TestFindArticleThrowingException()
        {
            var shopService = new ShopService();

            try
            {
                var actualArticle = shopService.FindArticle(1, 300);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, _findArticleCouldNotFind);
                return;
            }

            Assert.Fail("The expected exception was not thrown.");
        }

        [TestMethod]
        public void TestSellArticle()
        {
            var shopService = new ShopService();

            var actualArticle = new Article
            {
                Id = 1,
                Name = "Article from supplier1",
                Price = 458
            };
            var actualBuyerId = 1;

            var expectedArticle = new Article
            {
                Id = 1,
                Name = "Article from supplier1",
                Price = 458,
                IsSold = true,
                SoldDate = DateTime.Now
            };

            shopService.SellArticle(actualArticle, actualBuyerId);
            Assert.AreEqual(expectedArticle.Id, actualArticle.Id);
            Assert.AreEqual(expectedArticle.Name, actualArticle.Name);
            Assert.AreEqual(expectedArticle.Price, actualArticle.Price);
            Assert.AreEqual(expectedArticle.IsSold, actualArticle.IsSold);
            Assert.AreEqual(expectedArticle.SoldDate.ToString(), actualArticle.SoldDate.ToString());
        }

        [TestMethod]
        public void TestSellArticleThrowingException()
        {
            var shopService = new ShopService();
            var actualBuyerId = 1;
            
            try
            {
                shopService.SellArticle(null, actualBuyerId);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, _sellArticleArgumentNull);
                return;
            }

            Assert.Fail("The expected exception was not thrown.");
        }
    }
}
