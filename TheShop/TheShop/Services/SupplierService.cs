﻿using System.Collections.Generic;
using TheShop.Domains;

namespace TheShop.Services
{
    public class SupplierService
    {
        private readonly List<Supplier> _suppliers = new List<Supplier>();

        public SupplierService()
        {
            InitValues();
        }

        public List<Supplier> GetSuppliers()
        {
            return _suppliers;
        }

        private void InitValues()
        {
            var supplier1 = new Supplier();

            supplier1.AddArticle(new Article
            {
                Id = 1,
                Name = "Article from supplier1",
                Price = 458
            });

            _suppliers.Add(supplier1);

            var supplier2 = new Supplier();

            supplier2.AddArticle(new Article
            {
                Id = 1,
                Name = "Article from supplier2",
                Price = 459
            });

            _suppliers.Add(supplier2);

            var supplier3 = new Supplier();

            supplier3.AddArticle(new Article
            {
                Id = 1,
                Name = "Article from supplier3",
                Price = 460
            });

            _suppliers.Add(supplier3);
        }
    }
}
