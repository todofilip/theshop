﻿using System;
using System.Linq;
using TheShop.Db;
using TheShop.Domains;
using TheShop.Utilities;

namespace TheShop.Services
{
    public class ShopService
	{
		private readonly DatabaseDriver _databaseDriver;
		private readonly SupplierService _supplierService;
		private readonly Logger _logger;
		
		public ShopService()
		{
			_databaseDriver = new DatabaseDriver();
			_supplierService = new SupplierService();
			_logger = new Logger();
		}

		public Article FindArticle(int id, int maxExpectedPrice)
		{
			var article = _supplierService.GetSuppliers()
				.Where(s => s.ArticleInInventory(id) && s.GetArticle(id).Price < maxExpectedPrice)
				.Select(x => x.GetArticle(id))
				.FirstOrDefault();

			if (article == null)
			{
				throw new Exception("Could not find article");
			}

			return article;
		}

		public void SellArticle(Article article, int buyerId)
		{
			if (article == null)
			{
				throw new ArgumentNullException(nameof(article));
			}

			_logger.Debug($"Trying to sell article with Id={article.Id}");

			article.IsSold = true;
			article.BuyerUserId = buyerId;
			article.SoldDate = DateTime.Now;

			try
			{
				_databaseDriver.SaveArticle(article);
				_logger.Info($"Article with Id={article.Id} is sold.");
			}
			catch (Exception)
			{
				_logger.Error($"Could not save article with Id={article.Id}");
				throw new Exception("Could not save article");
			}
		}

		public Article GetArticleById(int id)
		{
			return _databaseDriver.GetArticleById(id);
		}
	}
}
