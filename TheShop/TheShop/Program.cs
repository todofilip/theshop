﻿using System;
using TheShop.Services;

namespace TheShop
{
	internal class Program
	{
		private static readonly ShopService _shopService = new ShopService();

		private static void Main(string[] args)
		{
			try
			{
				var article = _shopService.FindArticle(1, 2000);
				_shopService.SellArticle(article, 10);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}

			PrintArticleOnConsole(1);
			PrintArticleOnConsole(12);

			Console.ReadKey();
		}

		private static void PrintArticleOnConsole(int articleId)
		{
			try
			{
				var article = _shopService.GetArticleById(articleId);
				Console.WriteLine($"Found article with Id: {article.Id}");
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Article not found: {ex}");
			}
		}
	}
}