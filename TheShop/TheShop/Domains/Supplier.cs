﻿using System.Collections.Generic;
using System.Linq;

namespace TheShop.Domains
{
    public class Supplier
	{
		private readonly List<Article> _articles = new List<Article>();

		public bool ArticleInInventory(int id)
		{
			return _articles.FirstOrDefault(a => a.Id == id) != null;
		}

		public Article GetArticle(int id)
		{
			return _articles.FirstOrDefault(a => a.Id == id);
		}

		public void AddArticle(Article article)
		{
			_articles.Add(article);
		}
	}
}
