﻿using System.Collections.Generic;
using System.Linq;
using TheShop.Domains;

namespace TheShop.Db
{
    //in memory implementation
    public class DatabaseDriver
	{
		private readonly List<Article> _articles = new List<Article>();

		public Article GetArticleById(int id)
		{
			return _articles.Single(x => x.Id == id);
		}

		public void SaveArticle(Article article)
		{
			_articles.Add(article);
		}
	}
}
